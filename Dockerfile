FROM ubuntu:18.10

LABEL maintainer="Sergiu Marsavela <smarsavela@dathena.io>"

RUN apt-get update
RUN apt-get install -y python3 python3-dev python3-pip

# copy the current directory to the container as ./app
COPY ./ ./app
# set the working directory in the container to ./app
WORKDIR ./app

RUN pip3 install -r requirements.txt

# set env variables for python to work properly
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

# execute the Flask app
ENV FLASK_APP run.py
CMD ["flask", "run", "--host=0.0.0.0"]